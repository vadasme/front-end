export const state = () => ({
    api: {
        webservice: "api",
    },
})

export const actions = {
    async axiosPost({ state, getters }, payload) {
        let res = await this.$axios({
            method: "POST", headers: {}, url: payload.service, data: payload.data
        }).then(res => {
            return res
        }).catch(err => {
            let res = { data: undefined }
            // alert("Peticion con errores");
            return res;
        });
        return res;
    },
    async axiosGet({ state, getters }, payload) {
        let res = await this.$axios({
            method: "GET", headers: state.api.headers, url: payload.service, params: payload.params
        }).then(res => {
            return res
        }).catch(err => {
            let res = { data: undefined }
            // alert("Peticion con errores");
            return res;
        });
        return res;
    },
    async axiosPut({ state, getters }, payload) {
        let res = await this.$axios({
            method: "PUT", headers: {}, url: payload.service, data: payload.data
        }).then(res => {
            return res
        }).catch(err => {
            let res = { data: undefined }
            // alert("Peticion con errores");
            return res;
        });
        return res;
    },
    async axiosDelete({ state, getters }, payload) {
        let res = await this.$axios({
            method: "DELETE", headers: {}, url: payload.service, data: payload.data
        }).then(res => {
            return res
        }).catch(err => {
            let res = { data: undefined }
            // alert("Peticion con errores");
            return res;
        });
        return res;
    }
}

export const mutations = {

}

export const getters = {
    
}