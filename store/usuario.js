export const state = () => ({
    usuario: {},
    usuarios: [],
})

export const actions = {
    async obtenerUsuarios({ state, commit, dispatch, rootGetters },) {
        let path = "user/all"
        let response = await this.dispatch('service/axiosGet', { service: path, data: {} }).then(res => { return res });
        if (response.data != undefined) {
            commit("setUsuarios", response.data);
            return state.usuarios
        }
    },
    async guardarUsuario({ state, commit, dispatch, rootGetters },payload) {
        let path = "user/save"
        let response = await this.dispatch('service/axiosPost', { service: path, data: payload }).then(res => { return res });
        if (response.data != undefined) {
            commit("setUsuarios", response.data);
            return state.usuarios
        }
    },
    async editarUsuario({ state, commit, dispatch, rootGetters },payload) {
        let path = "user/edit/" + payload.id;
        let response = await this.dispatch('service/axiosPost', { service: path, data: payload }).then(res => { return res });
        if (response.data != undefined) {
            commit("setUsuarios", response.data);
            return state.usuarios
        }
    },
    async eliminarUsuario({ state, commit, dispatch, rootGetters },payload) {
        let path = "user/delete/" + payload.id;
        let response = await this.dispatch('service/axiosDelete', { service: path, data: {} }).then(res => { return res });
        if (response.data) {

        }
    },
}

export const getters = {
    getUsuario(state) {
        return state.usuario
    },
    getUsuarios(state) {
        return state.usuarios
    },
}

export const mutations = {
    setUsuario(state, data) {
        state.usuario = data;
    },
    setUsuarios(state, data) {
        state.usuarios = data;
    },
}
