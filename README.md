# Front-end en Nuxt.js

A continuacion se detalla como iniciar y ejecutar el front-end realizado en nuxt.js

Este front utiliza (consume) un api rest creada y seteada en el puerto 8200. Este api se incluye dentro del mismo proyecto.

## Inicializar front

```bash
# Para instalar depencias es necesario ejecutar este comando
$ npm install

# Una vez instaladas las dependencias, se ejecuta el perfil "Dev"
$ npm run dev

```
